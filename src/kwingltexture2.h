/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

Copyright (C) 2006-2007 Rivo Laks <rivolaks@hot.ee>
Copyright (C) 2010, 2011 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef KWIN_GLTEXTURE2_H
#define KWIN_GLTEXTURE2_H

#include <kwinglutils_export.h>

#include <QExplicitlySharedDataPointer>
#include <QMatrix4x4>
#include <QRegion>
#include <QSharedPointer>
#include <QSize>

#include <epoxy/gl.h>

#include <kwingltexture.h>

namespace KWin {

class GLTexturePrivate;

class GLTexture2 : public KWin::GLTexture
{
public:
    GLTexture2(GLenum internalFormat, int width, int height, int levels, GLuint texture);
    virtual ~GLTexture2();

protected:
    GLTexturePrivate *d_ptr;

private:
    GLTexturePrivate &createSetPrivate();
    Q_DECLARE_PRIVATE(GLTexture)
};

} // namespace KWin

#endif
