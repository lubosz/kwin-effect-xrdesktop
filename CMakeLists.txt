cmake_minimum_required (VERSION 3.16)
project (kwin-effects-xrdesktop
  VERSION 0.15.1
)

set (CMAKE_CXX_STANDARD 14)

INCLUDE (CPack)

include (FeatureSummary)

add_definitions (-Wall)
set (CMAKE_CXX_STANDARD_REQUIRED ON)
set (CMAKE_CXX_EXTENSIONS OFF)

find_package (ECM REQUIRED NO_MODULE)
set (CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH}
    ${ECM_MODULE_PATH}
    ${ECM_KDE_MODULE_DIR}
    ${CMAKE_SOURCE_DIR}/cmake
    ${CMAKE_SOURCE_DIR}/cmake/Modules
)

include (KDEInstallDirs)
include (KDECMakeSettings)

include (FindPkgConfig)

PKG_CHECK_MODULES (KWINXRDEPS REQUIRED
    xrdesktop-0.15
    graphene-1.0
    libinputsynth-0.16
)

include_directories(${KWINXRDEPS_INCLUDE_DIRS})
link_directories(${KWINXRDEPS_LIBRARY_DIRS})

# disable warnings for qt moc
SET(CMAKE_AUTOMOC   ON)
SET(CMAKE_AUTOMOC_MOC_OPTIONS "-nw")

if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    add_compile_options(
        -Wall -Wextra -Wpedantic -Wno-c99-extensions
    )
endif()

add_subdirectory (src)

message ("Include: ${KWINXRDEPS_INCLUDE_DIRS}")
message ("Link: ${KWINXRDEPS_LIBRARIES}")

feature_summary (WHAT ALL)
